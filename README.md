## Requisitos e instalação dos projetos

### <a name="indices">Índices</a>

- [Começando](#comecando)
- [Requisitos e configurações na máquina](#requisitos)
- [Como funciona o pattern repository](#como-funciona-pattern-repository)
- [Configurar git na máquina](#configurar-git-maquina)
- [Front-end local (na máquina do desenvolvedor)](#desenvolvimento-front-local)
- [Configurar projeto no servidor](#configurar-projeto-servidor)


## <a name="comecando">Começando</a>

Este documento tem por finalidade definir os requisitos para um desenvolvedor continuar com os projetos realizados nesta conta do GIT, por padrão, foi utilizado somente o laravel na versão 5.4, porém há um projeto que foi desenvolvido na versão 6 do laravel. 

## <a name="requisitos">Requisitos e configurações na máquina (local)</a>

- Desenvolvedor
  - Conhecimento avançado do framework [laravel](https://laravel.com/docs/5.4) na versão 5.4.
  - Domínio de html, html5, css e js.
  - Ter domínio das bibliotecas, [jQuery](https://jquery.com/) e do [bootstrap](https://getbootstrap.com/) na versão 4.3
  - Saber desenvolver no padrão de plugins do jQuery, ter no mínimo conhecimento médio, porque já tem um padrão de plugin, só irá precisar dar continuidade no padrão.
  - Ter experiência básica ou média do gulp js.
  - Ter experiência com git.
  - Estudar o pacote [pattern-repository](https://github.com/srgafanhoto/pattern-repository) que foi utilizado em todos os projetos. Caso algo não funcione, deixe seu comentário nas issues. Entenda mais sobre a utilizado neste tópico [Entenda o Pattern-repository](#como-funciona-pattern-repository)
- Máquina
  - Sistema operacional: Windows 64 bits
    - Não foi utilizado em momento algum o sistema linux, dessa forma, esse tutorial é apenas para o windows.
    - Caso deseja utilizar o linux, recomendamos que configure o docker.
  - PHP >= 7.2
    - OpenSSL PHP Extension
    - PDO PHP Extension
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - XML PHP Extension
  - Node.js >= 10.15
  - Git >= 2.19
  - Mysql >= 5.7.21 (Precisa do suport para o tipo de campo json)
  - Composer >= 1.8

Como são requisitos muito detalhados, foi separado todas as ferramentas e recursos para preparar seu ambiente de desenvolvimento no windows, basta acessar a pasta [ambiente-windows](./ambiente-windows) e instalar todos os programas, um detalhe importante, para prevenir erros durante a instalação, primeiramente, recomendamos que instale todos os pacotes do [Microsoft visual](./ambiente-windows/todos-microsoft-visual.zip), dentro do zip, existira duas pastas e um instalador, primeiramente execute o instalador, após isso, acesse cada pasta e instale um por vez, PRECISA FAZER COM AS DUAS PASTA SE O SEU WINDOWS FOR 64 BITS.

### <a name="como-funciona-pattern-repository">Como funciona o pattern repository</a>
Primeiramente, esteja ciente que o pacote [pattern-repository](https://github.com/srgafanhoto/pattern-repository) foi utilizado em todos os sistemas como padrão, porém, o pacote foi muito usado em projetos antes do pacote ser publicado, ou seja, o pacote na maioria dos projetos não foi pacote como pacote do laravel. Contudo, todos os projetos que estão utilizando o laravel 5.4, o pacote está dentro da pasta `packages/pattern-repository/` ou `packages/pattern/repository`. Já no laravel 6 foi utilizado o pacote deste [link](https://github.com/srgafanhoto/pattern-repository) que é exatamente o mesmo, Versões do pacote: `0.1 => Laravel = 5.4`, ``0.6 => Laravel = 6``.

Então basicamente, o backend do projeto está na seguinte estrutura junto com o pacote `View -> Controller -> Service -> Repository -> Model`. Explicando as etapas:
- `View`: seria os blades do laravel, onde o usuário dispara um ação que trocará de rota e passará a vez para o controller.
- `Controller`: Recebe a solicitação e se precisar, solicita dados para o `Service`, detalhe importante: Nem toda vez precisará passar para o service, isso seria por exemplo, na hora de cadastrar os dados de um formulário, caso for apenas uma listagem, pode acessar diretamente o `Repository`.
- `Service`: Usado para preparar os dados, validações e afins para passar para o `Repository`.
- `Repository`: Usado como intermediador de regras, CRUDS e afins. Exemplo de implementação do repository:
  - Você tem vários tipos de acessos no sistema, e precisa controlar eles por regras de usuários, ou seja, Administrador e Cliente, um exemplo de uso seria: `(new UserAdmRepository())->search()`, e dentro do repository você coloca a regra que deseja para apenas buscar os usuários que são administradores, dessa forma, previni diversos bugs e outros problemas que prejudicam a saúde do projeto.
- `Model`: O repository tem uma função onde aplica o Model pelo qual é responsável. 

### <a name="configurar-git-maquina">Configurar git na máquina</a>
O próprio gitlab tem uma ótima documentação de como criar um repositório git na sua máquina, tanto para um repositório existe no git quanto para um novo, acesse: https://gitlab.com/gabrielapr/config-init

### <a name="desenvolvimento-front-local">Front-end local (na máquina do desenvolvedor)</a>

Primeiramente acesse o arquivo `package.json` e o `gulpfile.js` e entenda o que foi instalado, como o tutorial é para dois projetos, verifique em qual versão do laravel do projeto você está desenvolvendo.

- Laravel 5.4
```bash
# Primeiramente, você precisa ter um conhecimento básico à mediano do gulp, porque foi utilizado ele como compilador. 
# O arquivos do front-end estão dentro da pasta resources/front e na compilação, serão jogadas para a pasta public.

# instalando dependencias
npm install

# Copiando dependencias para a pasta /public e executar o observador watch (Obrigatório na instalação do projeto pela primeira vez)
npm start

# watch - Não precisa executar toda vez o npm start para começar a desenvolver, para isso, rode o observador watch, toda vez que você salvar o arquivo, ele irá salvar automaticamente para a pasta public
npm run gulp watch

# Quando for subir o projeto para o servidor, rode o comando build, ele irá deixar os arquivos js e css limpos e minificados.
npm run gulp build

```
- Laravel 6
```bash
# Primeiramente, você precisa ter um conhecimento básico à mediano do webpack mix do laravel, porque foi utilizado ele como compilador. 
# O arquivos do front-end estão dentro da pasta resources/assets e na compilação, serão jogadas para a pasta public/assets.

# instalando dependencias
npm install

# Copiando dependencias para a pasta /public e executar o observador watch (Obrigatório na instalação do projeto pela primeira vez)
npm run dev

# watch - Não precisa executar toda vez o npm start para começar a desenvolver, para isso, rode o observador watch, toda vez que você salvar o arquivo, ele irá salvar automaticamente para a pasta public
npm run watch

```

### <a name="configurar-projeto-servidor">Configurar projeto no servidor</a>
Sempre tente utilizar o git no servidor, porque dessa forma, ao atualizar o projeto na sua máquina, você poderá somente dar um `git pull origin master` para atualizar o projeto do servidor.

1. Configurar o git no servidor.
    1. Primeiramente gere uma chave ssh no servidor no cpanel, segue os passos:
        1. Após acessar sua conta no cpanel, vá no bloco `Segurança` e depois em `Acesso a SSH`.
        2. Clique no botão `Manage SSH Keys` > `Generate a new Key`, preencha os dados e depois clique em `Gerar chave`.
        3. Volte para a listagem `Acesso a SSH`, encontre sua chave pública em `Public keys` e depois clique em `View/Download`, copie essa chave.
        4. Acesse sua conta git, vai em suas chaves e cole essa chave.
    1. Para configurar o git, acesse com o terminal ou ssh na pasta `public_html` do servidor e digite:
        1. `git config --global user.name "{seu_nome_do_git}"`
        2. `git config --global user.email "{seu_email_do_git}"`
        3. `git init`
        4. `git remote add origin git@gitlab.com:{seu_usuario_do_git}/{seu_repositorio_do_git}.git`
        5. `git pull origin master`
        6. Troque as informações dentro das chaves {} pelas suas.
         
2. Criar e-mail para realizar os envios de e-mails, configure os dados de acesso no arquivo ``.env``. Você pode configurar essa etapa direto pelo painel cpanel do servidor. Exemplo de configuração para o hostgator:
    1. 
    ```bash
   MAIL_DRIVER=mail
   MAIL_HOST=mail.exemplo.com.br
   MAIL_PORT=465
   MAIL_USERNAME=nao-responda@exemplo.com.br
   MAIL_PASSWORD="sua_senha_aqui"
   MAIL_ENCRYPTION=ssl
   MAIL_FROM_ADDRESS=nao-responda@exemplo.com.br
    ```
3. Criar conta gmail e salvar os dados em `_DOCS/configuracoes.txt`. Essa conta é utilizada para as configurações abaixo:
    1. Configurar recaptcha 
    2. Configurar google analytics
   
   

